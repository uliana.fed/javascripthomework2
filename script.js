let name = prompt("Whats`s your name?");
let age = parseInt(prompt("How old are you?"));

while (name === "" || isNaN(age)) {
    if (name === "") {
      name = prompt("Your answer is invalid, try again. What's your name?");
    }
  
    if (isNaN(age)) {
      age = prompt("Your answer is invalid, try again. How old are you?");
    }
  }

if(age<18) {
    alert("You are not allowed to visit this website")
} else if(age>=18 && age<=22){
    const accept = confirm("Are you sure you want to continue?");
    if(accept) {
        alert(`Welcome ${name}`)
    } else {
        alert("You are not allowed to visit this website");
    }
} else {
    alert(`Welcome ${name}`)
}